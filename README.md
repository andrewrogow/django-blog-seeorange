SeeOrange
======
![](http://i.prntscr.com/z2TuTSn-SSCvX1EwST9KZw.png)
======

## Для работы приложения необходим Python 2.7
.

![](http://i.prntscr.com/qkJZxGXKQjC14z1Xq4PU_g.png)

![](http://i.prntscr.com/DW1SSlw4RROWDgMMa1xggA.png)

![](http://i.prntscr.com/Iq3MzIqtQb6YiMJvrqLBrw.png)

![](http://i.prntscr.com/v0_118pGTCizD4FXgs51Hg.png)

## Настройка
1. Перейдите в папку с проектом `cd SeeOrange/`.
2. Установить зависимости из файла `requirements.txt`, выполнив команду `pip install -r requirements.txt`. Это автоматически установит Django и все нужные модули.
3. **В `settings.py` добавьте `blog` в список `INSTALLED_APPS` .**
4. Примените миграции к базе данных, выполнив команду `python manage.py migrate`<br>
5. Там же в `settings.py` вы можете настроить остальные параметры (язык, часовой пояс, и др.)
6. Укажите данные базы данных PostgreSQL или MySQL в `DATABASE_SETTINGS` в указанном формате. 
7. Запустите `python manage.py runserver`.
8. Приложение можно бесплатно захостить на Heroku.

еще пункт. Загрузите demo данные командой `python manage.py loaddata fixtures/entries.json`

## Доп информация
* Доп информация
* Доп информация
* Доп информация

