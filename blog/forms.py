# -*- coding:utf-8 -*-
from django import forms
from blog.models import Comment

class CommentFormForAnonymous(forms.ModelForm):
    class Meta:
        model = Comment
        fields =['email', 'author', 'text']



class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = 'text',
        labels = {'text': 'Введите текст комментария'}


