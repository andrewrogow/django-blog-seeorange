# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponseForbidden
from django.shortcuts import render, redirect
# from django.views.generic.detail import BaseDetailView, View
from django.views.generic import ListView, TemplateView, DetailView, CreateView
from django.views.generic.list import MultipleObjectMixin
from .models import Entry, Comment
from .forms import CommentForm, CommentFormForAnonymous
from django.core.exceptions import ImproperlyConfigured, PermissionDenied
# Create your views here.


class EntryQuerySetMixin(object):

    queryset = None

    def get_queryset(self):
        """
        Check that the queryset is defined and call it.
        """
        self.queryset = super(EntryQuerySetMixin, self).get_queryset()
        if self.queryset is None:
            raise ImproperlyConfigured(
                "'%s' must define 'queryset'" % self.__class__.__name__)

        if self.request.user.is_authenticated():
            return self.queryset.filter(status__in=[Entry.HIDDEN, Entry.PUBLISHED])
        else:
            return self.queryset.filter(status=Entry.PUBLISHED)




class EntryPreviewMixin(object):
    """
    Mixin implementing the preview of Entries.
    """
    object = None

    def get_object(self, queryset=None):
        """
        If the status of the entry is not PUBLISHED,
        a preview is requested, so we check if the user
        has the 'blog.can_view_all' permission or if
        it's an author of the entry.
        """
        self.object = super(EntryPreviewMixin, self).get_object(self.queryset)
        if self.object.is_visible_for_all:
            return self.object
        if self.object.is_visible_for_auth_users and self.request.user.is_authenticated():
            return self.object
        else:
            raise PermissionDenied('403 No entry found. Please auth or register')





class EntriesListView(EntryQuerySetMixin, ListView):
    template_name = 'feeds.html'
    model = Entry
    paginate_by = 5


class EntriesDetailView(EntryPreviewMixin, DetailView):
    template_name = 'detail.html'
    model = Entry
    pk_url_kwarg = 'entry_id'
    form = None

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            self.form = CommentForm()
        else:
            self.form = CommentFormForAnonymous()
        return super(EntriesDetailView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            form = CommentForm(request.POST)
        else:
            form = CommentFormForAnonymous(request.POST)

        if form.is_valid():
            comment = form.save(commit=False)
            comment.entries_id = kwargs[self.pk_url_kwarg]
            if isinstance(form, CommentForm):
                comment.author = request.user.get_short_name()
            form.save()
        return redirect('detail_entry', entry_id=kwargs[self.pk_url_kwarg])

    def get_context_data(self, **kwargs):
        context = super(EntriesDetailView, self).get_context_data(**kwargs)
        context['form'] = self.form
        context['comments'] = Comment.objects.filter(entries_id=self.get_object().id)
        return context


class HomeView(TemplateView):
    template_name = 'feeds.html'


