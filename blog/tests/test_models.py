# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

from django.test import TestCase
from django.contrib.auth.models import User
from blog.models import Entry


# TODO написать тесты для логики автопостинга, для логики создания предварительных статей, для проверки Entry.save()

class TestEntry(TestCase):
    """
    Testing methods, and next cases:
        -description is empty (needs auto create description)
        -description is not empty (nothing to do)

    """
    def test_auto_create_description_by_text(self):
        """
        description должен заполниться 300 первыми символами из text
        """
        params = {'title': 'PUBLUSHED',
                  'text': 'T' * 300,
                  'publication_date': datetime(2010, 1, 1, 23, 0),
                  'status': Entry.PUBLISHED}
        obj = Entry.objects.create(**params)

        self.assertEqual(len(obj.description), 303)

    def test_description_is_not_empty(self):
        """
        Если description был наран руками, ничего не предпренимать
        провреряем содержание description
        """
        params = {'title': 'PUBLUSHED',
                  'description': 'not empty',
                  'text': 'T' * 300,
                  'publication_date': datetime(2010, 1, 1, 23, 0),
                  'status': Entry.PUBLISHED}
        obj = Entry.objects.create(**params)

        self.assertEqual(obj.description, 'not empty')


    def test_is_visible_for_auth_users_method(self):
        """
        check 'Entry.is_visible_for_auth_users'
        """
        params = {'title': 'HIDDEN',
                  'text': 'Test text',
                  'publication_date': datetime(2010, 1, 1, 23, 0),
                  'status': Entry.HIDDEN}
        obj = Entry.objects.create(**params)

        self.assertEqual(obj.is_visible_for_auth_users, True)

    def test_is_visible_for_all_method(self):
        """
        check 'Entry.is_visible_for_all'
        """
        params = {'title': 'HIDDEN',
                  'text': 'Test text',
                  'publication_date': datetime(2010, 1, 1, 23, 0),
                  'status': Entry.PUBLISHED}
        obj = Entry.objects.create(**params)

        self.assertEqual(obj.is_visible_for_all, True)

