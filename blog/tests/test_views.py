# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

from django.test import TestCase, Client
from django.contrib.auth.models import User
from blog.models import Entry


class ViewBaseCase(TestCase):

    """
    Create: test user, all cases entries:

        -available for all (Entry.PUBLISHED) "entry_for_all"
        -available for auth users (Entry.HIDDEN) "entry_for_auth_users"
        -not available for all users (except Administrator in admin panel) "not_posted_entry"
    """

    username = 'test_user'
    email = 'test_user@test.com'
    password = 'password'

    def setUp(self):
        self.author = User.objects.create_user(username=self.username,
                                               email=self.email,
                                               password=self.password)
    def create_PUBLISHED_entry(self):
        params = {'title': 'PUBLUSHED',
                  'text': 'test-1, -available for all (Entry.PUBLISHED) "entry_for_all"',
                  'publication_date': datetime(2010, 1, 1, 23, 0),
                  'status': Entry.PUBLISHED}
        entry_for_all = Entry.objects.create(**params)
        return entry_for_all

    def create_HIDDEN_entry(self):
        params = {'title': 'HIDDEN',
                  'text': '-available for auth users (Entry.HIDDEN) "entry_for_auth_users"',
                  'publication_date': datetime(2010, 5, 31, 23, 00),
                  'status': Entry.HIDDEN}
        entry_for_auth_users = Entry.objects.create(**params)
        return entry_for_auth_users

    def create_TO_PUBLICATION_entry(self):
        params = {'title': '',
                  'text': '-not available for all users (except Administrator in admin panel) "not_posted_entry"',
                  'publication_date': datetime(2010, 5, 31, 23, 00),
                  'status': Entry.TO_PUBLICATION}
        not_posted_entry = Entry.objects.create(**params)
        return not_posted_entry

    # TODO: пофиксить время публикации, время будущей публикации, поля которые сейвятся/апдейтятся во время сохранения
    # продумать всю логику в МОДЕЛИ
    def create_published_entry(self):
        params = {
            'title': 'TO_PUBLICATION',
            'description ': 'Test Description',

            'text': 'Test entry',
            'slug': 'test-4',

            'status': Entry.PUBLISHED,

            'publication_date': datetime(2010, 1, 1, 23, 0),
            'start_publication': datetime(2010, 1, 1, 23, 0),
            'creation_date': datetime(2010, 1, 1, 23, 0),
            'last_update': datetime(2010, 1, 1, 23, 0),
            'is_commentable': True,
            'user': self.author,
            'tags': 'test'
        }
        return params

    def check_publishing_context(self, url, first_expected,
                                 second_expected=None,
                                 friendly_context=None,
                                 queries=None):
        """Test the numbers of entries in context of an url."""
        if queries is not None:
            with self.assertNumQueries(queries):
                response = self.client.get(url)
        else:
            response = self.client.get(url)
        self.assertEqual(len(response.context['object_list']),
                         first_expected)
        if second_expected:
            self.create_published_entry()
            response = self.client.get(url)
            self.assertEqual(len(response.context['object_list']),
                             second_expected)
        if friendly_context:
            self.assertEqual(
                response.context['object_list'],
                response.context[friendly_context])
        return response


class ViewsTastCase(ViewBaseCase):
    """
    Test all cases
    """
    def test_EntriesListView_get(self):
        """
        check: the /blog/ URL is found
        """
        response = self.client.get('/blog/')

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

    def test_check_available_for_all_entries(self):
        """"
        User: anonymous
        Entries: 1 PUBLISHED and 1 HIDDEN
        Check that the rendered context contains 1 entries.
        """

        self.create_PUBLISHED_entry()
        self.create_HIDDEN_entry()

        response = self.client.get('/blog/')
        self.assertEqual(len(response.context['object_list']), 1)

    def test_check_available_for_auth_users(self):
        """"
        User: auth users
        Entries: 1 PUBLISHED and 1 HIDDEN
        Check that the rendered context contains 2 entries.
        (for all and for auth users (PUBLISHED and HIDDEN))
        """

        self.create_PUBLISHED_entry()
        self.create_HIDDEN_entry()

        self.client.force_login(self.author)
        response = self.client.get('/blog/')
        self.assertEqual(len(response.context['object_list']), 2)

    def test_check_not_available_entries_for_anonumus(self):
        """"
        User: anonymous
        Entries: 1 TO_PUBLICATION
        Check that the rendered context contains 0 entries.
        """
        self.create_TO_PUBLICATION_entry()

        response = self.client.get('/blog/')
        self.assertEqual(len(response.context['object_list']), 0)

    def test_check_not_available_entries_for_auth_user(self):
        """"
        User: auth_user
        Entries: 1 TO_PUBLICATION
        Check that the rendered context contains 0 entries.
        """
        self.create_TO_PUBLICATION_entry()

        self.client.force_login(self.author)
        response = self.client.get('/blog/')
        self.assertEqual(len(response.context['object_list']), 0)