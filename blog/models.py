# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

from django.db import models
from django.contrib.auth.models import User
from taggit.managers import TaggableManager
from django.utils.html import format_html

class Entry(models.Model):
    """
    Entry model class  providing
    the fields and methods required for publishing
    """
    TO_PUBLICATION = 0
    HIDDEN = 1
    PUBLISHED = 2
    STATUS_CHOICES = (
        (TO_PUBLICATION, 'к публикации'),
        (HIDDEN, 'скрытая'),
        (PUBLISHED, 'опубикована')
    )

    title = models.CharField(verbose_name='заголовок', max_length=255)  #
    description = models.TextField(verbose_name='краткое содержание',
                                   help_text='краткое описание статьи. Будет отображаться в ленте', blank=True)
    text = models.TextField(verbose_name='содержание статьи')
    # slug = models.SlugField(verbose_name='slug', max_length=255, unique=True,
    #                         help_text="используется для создания URL.")
    status = models.IntegerField(verbose_name='статус статьи', choices=STATUS_CHOICES, default=TO_PUBLICATION)  #

    publication_date = models.DateTimeField(verbose_name='дата публикации',
                                            help_text="дата когда опубликовали статью.", auto_now_add=True, null=True)
    start_publication_date = models.DateTimeField(verbose_name='Публикуемая дата', blank=True, null=True,
                                                  help_text='дата когда опубликуется статья.', default=None)
    creation_date = models.DateTimeField(verbose_name='дата создания', auto_now_add=True,
                                         help_text="дата написания статьи.", null=True, blank=True)
    last_update_date = models.DateTimeField(verbose_name='последний раз обовлено', auto_now=True)
    is_commentable = models.BooleanField(default=True, verbose_name="Комментарии разрешены",
                                         help_text='позволяет отключить комментарии')  #
    comment = models.ForeignKey('blog.Comment', blank=True, null=True)
    user = models.ForeignKey(User, verbose_name='автор статьи', editable=True, null=True)
    tags = TaggableManager(blank=True, verbose_name="Теги")

    class Meta:
        ordering = ["-publication_date"]

    def __unicode__(self):
        return 'Entry #{} {}'.format(self.pk, self.title)

    # @property
    # def is_actual(self):
    #     return True

    @property
    def is_visible_for_all(self):
        """
        проверяет если запись published.
        """
        return self.status == self.PUBLISHED

    @property
    def is_visible_for_auth_users(self):
        """
        проверяет если запись видима только для авторизированных пользователей
        """
        return self.status == self.HIDDEN

    def create_description(self):
        """
        auto create short description if not wrote
        """
        if self.text and not self.description:
            self.description = self.text[:300] + '...'

    def colored_status(self):
        """
        Colored status for Admin panel
        """
        colors = {
            'red':'D60004',
            'grey':'625E65',
            'green':'179225'}

        # statuses have colors
        # PUBLISHED is green
        # HIDDEN is grey

        # TO_PUBLICATION status is red
        color = colors['red']

        if self.status == Entry.PUBLISHED:
            color = colors['green']
        elif self.status == Entry.HIDDEN:
            color = colors['grey']
        return format_html(
            '<span style="color: #{};"><b>{}</b></span>',
            color,
            self.STATUS_CHOICES[self.status][1],
        )

    def save(self, *args, **kwargs):

        # заполнить поле description автоматически,
        # если это не было сделано вручную.
        self.create_description()
        super(Entry, self).save(*args, **kwargs)


class Comment(models.Model):
    entries = models.ForeignKey('blog.Entry', related_name='+', null=True)
    author = models.CharField(max_length=200)
    email = models.EmailField()
    text = models.TextField(verbose_name='коментарий')
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_date']

    def __unicode__(self):
        return self.text
