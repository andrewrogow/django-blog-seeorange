# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Entry, Comment


# class EntryAdmin(admin.ModelAdmin):
#     """
#     Admin for Entry model.
#     """
#

admin.site.register(Entry)
admin.site.register(Comment)

